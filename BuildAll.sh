#!/bin/sh

#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#

echo "Building all Docker image(s) locally..."

docker build -t "nhbs/bimandscan-conan-build:gcc8-x64" "./gcc8-x64"
docker build -t "nhbs/bimandscan-conan-build:clang7-x64" "./clang7-x64"
docker build -t "nhbs/bimandscan-conan-build:runtime" "./runtime"
