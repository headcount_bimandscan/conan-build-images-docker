# BIM & Scan® - Conan Build Images (Docker)

Docker images with Conan support for building C/C++ software. Similar in purpose to the [conanio](https://hub.docker.com/u/conanio/) releases they are based on. Images contain the default [conan-center](https://bintray.com/conan/conan-center) remote, as well as [bincrafters](https://bintray.com/bincrafters/public-conan). Linux-based builds use [Ubuntu](https://www.ubuntu.com/) as their base.

Released under the terms of the Apache Licence v2.0, see "LICENCE.md" in the project root for more information.

This project contains scripts for creating the following images...

* 'gcc8-x64' - Linux (x64) build using GCC v8.* with the 'libstdc++11' ABI.
* 'clang7-x64' - Linux (x64) build using Clang 7.*.
* 'runtime' - Linux (x64) build for runtime images (no build tooling installed).

Build type configuration is accomplished through the use of Conan profiles. 'Debug', 'RelWithDebInfo', and 'Release', profiles are installed into these images by default. Profile names correspond to build type names.
